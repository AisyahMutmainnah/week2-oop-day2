<?php

class Animal
{
  public $name;
  public $legs = 4;
  public $coolBlooded = "no";

  public $yell = "Auooo";

  public function __construct($string)
  {
    $this->name = $string;
  }
}
